import { Certificate } from '@fidm/x509';
import _sortBy from 'lodash/sortBy';

import {formatDate} from './src/formatters';
import {injectPlugin} from "@floocus/spider-plugin";

function extractAttributes(attr){
    return _sortBy(attr.map(a => ({label: a.name, value: a.value})), ['label']);
}

async function certificateDecoder({
   inputs: {part, header, value},
   parameters: {headerName},
   callbacks: {setDecodedHeaders, onShowInfo, onShowError, onShowWarning },
   libs: {React}
}){

    if (header === headerName) {
        const decoded = [];

        //decode url encoded
        //repair value if all in one line
        const PEM = decodeURIComponent(value).replace(/(?<!-----(?:BEGIN|END))(\s+)/g, '\n');

        const certificate = Certificate.fromPEM(PEM);
        const issuer = extractAttributes(certificate.issuer.attributes);
        const subject = extractAttributes(certificate.subject.attributes);

        decoded.push({
            key: 'certificate subject',
            value: (
                <>
                    {subject.map(attr => (
                        <div key={attr.label}>
                            - {attr.label}: {attr.value}
                        </div>
                    ))}
                </>
            ),
            format: 'react',
            decoded: true
        });
        decoded.push({
            key: 'certificate issuer',
            value: (
                <>
                    {issuer.map(attr => (
                        <div key={attr.label}>
                            - {attr.label}: {attr.value}
                        </div>
                    ))}
                </>
            ),
            format: 'react',
            decoded: true
        });
        decoded.push({
            key: 'certificate validity',
            value: `${formatDate(certificate.validFrom)} ⟶ ${formatDate(certificate.validTo)}`,
            format: 'text/plain',
            decoded: true
        });

        setDecodedHeaders(decoded);
    }
}

injectPlugin({
    id: 'x-ssl-cert-decoder',
    type: 'http-headers-decode-plugin',
    version: '1.1',
    func: certificateDecoder,
    errorMessage: 'Could not decode certificate!'
});