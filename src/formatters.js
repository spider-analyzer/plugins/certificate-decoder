export function formatDate(date) {
    return date.getFullYear() +
        '-' + pad( date.getMonth() + 1 ) +
        '-' + pad( date.getDate() ) +
        ' ' + pad( date.getHours() ) +
        ':' + pad( date.getMinutes() ) +
        ':' + pad( date.getSeconds() );
}

function pad(number) {
    if ( number < 10 ) {
        return '0' + number;
    }
    return number;
}
